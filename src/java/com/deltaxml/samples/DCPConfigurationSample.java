// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DCPConfiguration;
import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DynamicPDFormatException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PDAdvancedConfigException;
import com.deltaxml.cores9api.PDFilterConfigurationException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.StaticPDFormatException;

public class DCPConfigurationSample {
  
  public static void main(String[] args) {
    
    File input1= new File("input1.xml");
    File input2= new File("input2.xml");
    File resultFolder= new File("result");
    File dcpDefinition= new File("diffreport.dcp");
    File dcResultFolder= new File(resultFolder, "DCP_API");
    
    Map<String, String> dcpStringParams= new HashMap<String, String>();
    dcpStringParams.put("formatting-element-list", "b,i,u");
    Map<String, Boolean> dcpBooleanParams= new HashMap<String, Boolean>();
    dcpBooleanParams.put("convert-to-html", true);
    
    dcResultFolder.mkdirs();
    
    try {
    System.out.println("Initialising configuration");
    DCPConfiguration dcp= new DCPConfiguration(dcpDefinition);
    
    System.out.println("Generating new DocumentComparator with configuration 1");
    dcp.generate(dcpBooleanParams, dcpStringParams);
    
    DocumentComparator comparator = dcp.getDocumentComparator();   
    
    System.out.println("Starting comparison");
    File result1= new File(dcResultFolder, "result1.html");
    comparator.compare(input1, input2, result1);
    System.out.println("Comparison complete. Result 1 output to: " + result1.getAbsolutePath());
    
    System.out.println("Modifying DocumentComparator for configuration 2");
    dcpBooleanParams.put("convert-to-html", false);
    dcp.setParams(dcpBooleanParams, dcpStringParams);  
    
    System.out.println("Starting comparison");
    File result2= new File(dcResultFolder, "result2.xml");
    comparator.compare(input1, input2, result2);
    System.out.println("Comparison complete. Result 2 output to: " + result2.getAbsolutePath());
    
    } catch (StaticPDFormatException e) {
      System.out.println(e.getMessage());
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    } catch (PDFilterConfigurationException e) {
      System.out.println(e.getMessage());
    } catch (DynamicPDFormatException e) {
      System.out.println(e.getMessage());
    } catch (PDAdvancedConfigException e) {
      System.out.println(e.getMessage());
    } catch (IllegalStateException e) {
      System.out.println(e.getMessage());
    } catch (ComparisonException e) {
      System.out.println(e.getMessage());
    } catch (FilterProcessingException e) {
      System.out.println(e.getMessage());
    } catch (PipelineLoadingException e) {
      System.out.println(e.getMessage());
    } catch (PipelineSerializationException e) {
      System.out.println(e.getMessage());
    } catch (LicenseException e) {
      System.out.println(e.getMessage());
    } catch (PipelinedComparatorError e) {
      System.out.println(e.getMessage());
    } catch (IOException e) {
      System.out.println(e.getMessage());
    } catch (ComparisonCancelledException e) {
      System.out.println(e.getMessage());
    }
  }  
}
