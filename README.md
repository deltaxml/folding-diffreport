# Folding DiffReport with DCP
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

A demonstration of DCP, showcasing the capabilities of the folding DiffReport output format.

This document describes how to run the sample. For concept details see: [Folding DiffReport with DCP](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/folding-diffreport-with-dcp)

## Using the Command-line interface
If you are using any Java version of XML Compare and have Apache Ant installed, use the build script provided to run the command-line sample. Simply type the following command. This will generate output for all three methods in the results directory.

	ant

Also using the Apache Ant build script provided, you can simply type the following command to generate output for the two command line DCP methods in the results directory.

	ant run-dcp-command
	
If you don't have Ant installed, you can run the samples from a command line by issuing commands from the sample directory (ensuring that you use the correct directory and class path separators for your operating system):

	java -jar ../../deltaxml-x.y.z.jar compare dcp-folding input1.xml input2.xml result.html formatting-element-list=b,i,u convert-to-html=true
	java -jar ../../deltaxml-x.y.z.jar compare dcp-folding input1.xml input2.xml result.xml formatting-element-list=b,i,u convert-to-html=false
	
*Note: replace the 'x.y.z' in deltaxml-x.y.z.jar with the major.minor.patch version number of your release e.g. command-10.0.0.jar*
    
## With DCPConfiguration Java API
The DCPConfiguration class is used to manage DCP configurations using the Java API. See the code in the DCPConfigurationSample.java source file.  The following command is used to compile and run this API sample using Apache Ant.

	ant run-dcp-api
